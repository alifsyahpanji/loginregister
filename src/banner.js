import banner from './assets/banner.png';

function BannerComponent() {
    return (
        <div className='container-fluid d-flex flex-column justify-content-center align-items-center'>
            <div className='mt-3 pt-3'><img src={banner} alt="banner" /></div>
            <div className='bannerwelcometitle text-center mt-4'>
                Hai, Fren!
            </div>
            <div className='bannerwelcometext'>Selamat Datang di Aplikasi WoiShop</div>
        </div>

    );
}

export default BannerComponent;