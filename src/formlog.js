import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Link } from "react-router-dom";
import { ReactComponent as SvgPerson } from './assets/person.svg';
import { ReactComponent as SvgLock } from './assets/lock.svg';

function FormLogComponent() {
    const navigateLogin = useNavigate();

    const [userInput, setUserInput] = useState('kminchelle');
    const [passInput, setPassInput] = useState('0lelplR');


    const handleSubmit = event => {


        event.preventDefault();

        fetch('https://dummyjson.com/auth/login', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({

                username: userInput,
                password: passInput,
                // expiresInMins: 60, // optional
            })
        })
            .then((res) => res.json())
            .then((data) => {
                if (userInput === data.username) {
                    const fullNameApi = data.firstName + " " + data.lastName;
                    localStorage.setItem("usernameLog", data.username);
                    localStorage.setItem("fullNameLog", fullNameApi);
                    localStorage.setItem("imageLog", data.image);
                    localStorage.setItem("emailLog", data.email);
                    localStorage.setItem("genderLog", data.gender);
                    navigateLogin('/profil');
                }
            }
            );




    };

    return (
        <div className='container-fluid d-flex justify-content-center'>
            <form className='formsize' onSubmit={handleSubmit}>
                <div className="input-group mt-3">
                    <span className="input-group-text"><SvgPerson /></span>
                    <input type="text" id="userLog" className="form-control" placeholder="Username" onChange={event => setUserInput(event.target.value)}
                        value={userInput} />
                </div>

                <div className="input-group mt-3">
                    <span className="input-group-text"><SvgLock /></span>
                    <input type="text" id="passLog" className="form-control" placeholder="Password" value={passInput}
                        onChange={event => setPassInput(event.target.value)} />
                </div>

                <div className="d-grid gap-2 mt-3">
                    <button className="btn btn-green" type="submit" >Login</button>
                </div>

                <div className="mt-3 text-center askaccount">Belum punya akun? <span className="asklink"><Link to="/register">Daftar Di Sini</Link></span></div>
            </form>
        </div>
    );
}




export default FormLogComponent;