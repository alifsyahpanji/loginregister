import '../style.css';
import ProfilHeader from './profilheader.js';
import ProfilMenu from './profilmenu.js';
import { Navigate } from "react-router-dom";


function Profil() {

    const checkUser = localStorage.getItem("usernameLog");

    if (checkUser) {
        return (
            <div>
                <ProfilHeader />
                <ProfilMenu />
            </div>
        );
    } else {
        return <Navigate to="/" />;
    }


}

export default Profil;