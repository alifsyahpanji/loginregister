import iconUbahProfil from './assets/iconubahprofil.png';
import iconAjakTeman from './assets/iconajakteman.png';
import iconPoin from './assets/iconpoin.png';
import iconKetentuan from './assets/iconketentuan.png';
import iconKebijakan from './assets/iconkebijakan.png';
import iconBantuan from './assets/iconbantuan.png';
import textButton from './assets/textbutton.png';
import React from "react";
import { useNavigate } from 'react-router-dom';

function ProfilMenu() {

    let navigateLogout = useNavigate();

    const Logout = () => {
        localStorage.removeItem("usernameLog");
        localStorage.removeItem("fullNameLog");
        localStorage.removeItem("imageLog");
        localStorage.removeItem("emailLog");
        localStorage.removeItem("genderLog");
        navigateLogout("/");
    };



    return (
        <div className="d-flex flex-column justify-content-center align-items-center">
            <div className="profilmenu py-3 px-3">Akun</div>
            <div className="menu">

                <div className='d-flex'>
                    <span className='px-2'><img src={iconUbahProfil} alt="ubah profil" /></span>
                    <span className='me-auto'>Ubah Profil</span>
                    <span className='px-2'><img src={textButton} alt="text button" /></span>
                </div>

                <div className='d-flex mt-3'>
                    <span className='px-2'><img src={iconAjakTeman} alt="ajak temanl" /></span>
                    <span className='me-auto'>Ajak Teman</span>
                    <span className='px-2'><img src={textButton} alt="text button" /></span>
                </div>

                <div className='d-flex mt-3'>
                    <span className='px-2'><img src={iconPoin} alt="poin" /></span>
                    <span className='me-auto'>Poin</span>
                    <span className='px-2'><img src={textButton} alt="text button" /></span>
                </div>

                <div className='d-flex mt-3'>
                    <span className='px-2'><img src={iconKetentuan} alt="ketentuan layanan" /></span>
                    <span className='me-auto'>Ketentuan Layanan</span>
                    <span className='px-2'><img src={textButton} alt="text button" /></span>
                </div>

                <div className='d-flex mt-3'>
                    <span className='px-2'><img src={iconKebijakan} alt="kebijakan privasi" /></span>
                    <span className='me-auto'>Kebijakan Privasi</span>
                    <span className='px-2'><img src={textButton} alt="text button" /></span>
                </div>

                <div className='d-flex mt-3'>
                    <span className='px-2'><img src={iconBantuan} alt="bantuan" /></span>
                    <span className='me-auto'>Bantuan</span>
                    <span className='px-2'><img src={textButton} alt="text button" /></span>
                </div>


                <div className='d-flex justify-content-center mt-5'>
                    <button type="submit" className="btn btnlogout" onClick={Logout}>Logout</button>

                </div>

            </div>
        </div>
    );
}



export default ProfilMenu;