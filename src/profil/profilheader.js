import { ReactComponent as SvgEmail } from './assets/email.svg';
import { ReactComponent as SvgUsername } from './assets/person.svg';
import { ReactComponent as SvgGender } from './assets/gen.svg';

function ProfilHeader() {

    const username = localStorage.getItem("usernameLog");
    const fullName = localStorage.getItem("fullNameLog");
    const image = localStorage.getItem("imageLog");
    const email = localStorage.getItem("emailLog");
    const gender = localStorage.getItem("genderLog");




    return (
        <div>
            <div className="container-fluid profilheader">
                <div className="profiltext pt-4">Profil</div>

                <div className="d-flex flex-column justify-content-center align-items-center">

                    <div className="mt-2">
                        <img src={image} alt="profilimg" className="profilimg img-fluid rounded-circle" />
                    </div>

                    <div className="profilname mt-3">{fullName}</div>
                    <div className="profilemail mt-3"><span className='me-2'><SvgEmail /></span>{email}</div>
                    <div className="profilemail mt-3"><span className='me-2'><SvgUsername /></span>{username}</div>
                    <div className="profilemail mt-3"><span className='me-2'><SvgGender /></span>{gender}</div>


                </div>

            </div>
        </div>

    );
}

export default ProfilHeader;