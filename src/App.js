import './style.css';
import BannerComponent from './banner.js';
import FormLogComponent from './formlog.js';

function App() {
  return (
    <div>
      <BannerComponent />
      <FormLogComponent />
    </div>
  );
}

export default App;
