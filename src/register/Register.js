import '../style.css';
import BannerComponent from '../banner.js';
import FormRegComponent from './formreg.js';

function Register() {
    return (
        <div>
            <BannerComponent />
            <FormRegComponent />

        </div>
    );
}

export default Register;