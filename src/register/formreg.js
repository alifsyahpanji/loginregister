import { useState } from 'react';
import { Link } from "react-router-dom";

import { ReactComponent as SvgPerson } from './assets/person.svg';
import { ReactComponent as SvgEmail } from './assets/email.svg';
import { ReactComponent as SvgLock } from './assets/lock.svg';

function FormRegComponent() {

    const [userInputReg, setUserInputReg] = useState('');
    const [emailInputReg, setEmailInputReg] = useState('');
    const [passInputReg, setPassInputReg] = useState('');


    const handleSubmit = event => {


        event.preventDefault();

        console.log(userInputReg);
        console.log(emailInputReg);
        console.log(passInputReg);

        setUserInputReg('');
        setEmailInputReg('');
        setPassInputReg('');

    };

    return (
        <div className='container-fluid d-flex justify-content-center'>
            <form className='formsize' onSubmit={handleSubmit}>
                <div className="input-group mt-3">
                    <span className="input-group-text"><SvgPerson /></span>
                    <input type="text" id="userLog" className="form-control" placeholder="Username" onChange={event => setUserInputReg(event.target.value)}
                        value={userInputReg} />
                </div>

                <div className="input-group mt-3">
                    <span className="input-group-text"><SvgEmail /></span>
                    <input type="email" id="userLog" className="form-control" placeholder="Email" onChange={event => setEmailInputReg(event.target.value)}
                        value={emailInputReg} />
                </div>

                <div className="input-group mt-3">
                    <span className="input-group-text"><SvgLock /></span>
                    <input type="password" id="passLog" className="form-control" placeholder="Password" value={passInputReg}
                        onChange={event => setPassInputReg(event.target.value)} />
                </div>

                <div className="d-grid gap-2 mt-3">
                    <button className="btn btn-green" type="submit" >Register</button>
                </div>

                <div className="mt-3 text-center askaccount">Sudah punya akun? <span className="asklink"><Link to="/">Login Di Sini</Link></span></div>

            </form>



        </div>
    );
}




export default FormRegComponent;